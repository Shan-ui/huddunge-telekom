/**
 * @project - Huddunge Telekom
 * @category   Javascript / jQuery /
 * @date -5th March 2016
 * @author     Shan Dhiviyarajan - <prashasoft@gmail.com>
 */

'use strict';

/**
 * Define Global namespace for app
 */
var telecomApp = telecomApp || {};
//Easing value//
var easing = 'easeOutExpo';
// Google map canvas container
var googleMapCanvas = $("#google-map-canvas");

(function ($) {
    /**
     * Header
     * @type {{init: telecomApp.header.init}}
     */
    telecomApp.header = {

    };
    /**
     * Sidebar left
     * @type {{init: telecomApp.sidebarLeft.init, sideMenu: telecomApp.sidebarLeft.sideMenu, hideBarLeft: telecomApp.sidebarLeft.hideBarLeft}}
     */
    telecomApp.sidebarLeft = {
        init: function () {
            this.sideMenuCollapse();
            this.hideLeftSideBar();
        },

        /**
         * Side menu left collapse
         */
        sideMenuCollapse: function () {
            $(".menu-left > nav > ul > li").on('click', function (e) {

                if ($(".menu-left > nav > ul > li").find("ul").length > 0) {
                    $(".menu-left > nav > ul > li").not($(this)).removeClass("active").find("ul").slideUp(300, easing);
                    $(this).find("ul").slideDown(300, easing);
                    $(this).addClass("active");
                }

            });

        },

        /**
         * Hide left side bar
         */
        hideLeftSideBar: function () {
            setTimeout(function () {
                $("#sidebar-left").toggleClass("hide-sidebar-left");
            }, 700);
            $(".toggle-sidebar-left").on('click', function () {

                $("#sidebar-left").toggleClass("hide-sidebar-left");
            })
        }
    };
    /**
     * Sidebar right
     * @type {{init: telecomApp.slidebarRight.init, hideSideBar: telecomApp.slidebarRight.hideSideBar, collapse: telecomApp.slidebarRight.collapse, scrollParametersLayers: telecomApp.slidebarRight.scrollParametersLayers, collaplseTeamInfo: telecomApp.slidebarRight.collaplseTeamInfo}}
     */
    telecomApp.slidebarRight = {
        init: function () {
            this.hideSideBar();
            this.collapse();
            this.collapseTeamInfo();
        },

        /**
         * Hide side bar right
         */
        hideSideBar: function () {
            setTimeout(function () {
                $("#sidebar-right").toggleClass("hide-sidebar-right");
            }, 700);

            $(".toggle-sidebar-right").on('click', function () {

                $(this).find("span").toggleClass("glyphicon-menu-right glyphicon-menu-left");
                setTimeout(function () {
                    $("#sidebar-right").toggleClass("hide-sidebar-right");
                }, 200);

            });
        },

        /**
         * Collapse panes inside home  and team container
         */
        collapse: function () {

            $("#home .tab-pane > section > h4, #team.tab-pane > section > h4").on('click', function () {
                $(this).toggleClass("active");
                $(this).parent().find("> div").slideToggle(300, easing);
            });

        },

        /**
         * Collapse team information
         */
        collapseTeamInfo: function () {
            $(".team-info").find(".t-header").on('click', function () {
                $(this).parent().parent().toggleClass("active");
                $(this).parent().find(".t-body").slideToggle(300, easing);
            });
        }
    };
    /**
     * Sidebar top
     * @type {{init: telecomApp.sidebarTop.init, showHideSidebarTop: telecomApp.sidebarTop.showHideSidebarTop}}
     */
    telecomApp.sidebarTop = {
        init: function () {
            this.showHideSidebarTop();
        },
        /**
         * show hide sidebar top
         */
        showHideSidebarTop: function () {
            $(".toggle-sidebar-top").on('click', function () {
                $("#sidebar-top").toggleClass("hide-sidebar-top");
            });
        }
    };
    /**
     * Google Map
     * @type {{init: telecomApp.GoogleMap.init, fullScreenMap: telecomApp.GoogleMap.fullScreenMap, loadMap: telecomApp.GoogleMap.loadMap}}
     */
    telecomApp.GoogleMap = {
        init: function () {
            this.loadMap();
            this.fullScreenMap();

        },

        fullScreenMap: function () {

            // Screen width and Height
            googleMapCanvas.width($(window).width()).height($(window).height());

            // Screen width and Height with resize
            $(window).resize(function () {
                googleMapCanvas.width($(window).width()).height($(window).height());
            });
        },

        loadMap: function () {
            var center = new google.maps.LatLng(40.7055651, -74.1180841);
            var mapOptions = {
                center: center,
                zoom: 11,
                zoomControl: true,
                navigationControl: false,
                scaleControl: true,
                scrollwheel: true,
                mapTypeControl: true
            };
            //40.7055651,-74.1180841,11z
            // var mapMarker = "/pressing/wp-content/themes/pressing/assets/images/mapMarker.png";
            //var infowindow = new google.maps.InfoWindow({
            //    content: '<div class="results-info-window"><a href="#"><h4>' + mapData.name + '</h4><p>' + mapData.address + '</p></a></div>'
            //});

            if ($("#google-map-canvas").length > 0) {
                var map = new google.maps.Map(document.getElementById("google-map-canvas"), mapOptions);
                var marker = new google.maps.Marker({
                    position: center,
                    map: map,
                    animation: google.maps.Animation.DROP,
                    clickable: true
                });
                /**
                 * Google map marker click
                 */
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
                /**
                 * Contianer resize center the marker...
                 */
                google.maps.event.addDomListenerOnce(map, 'idle', function () {
                    google.maps.event.addDomListener(window, 'resize', function () {
                        map.setCenter(center);
                    });
                });
            }
        }

    };
    /**
     * App initialization
     */
    telecomApp.init = function () {

        this.sidebarTop.init();
        this.sidebarLeft.init();
        this.slidebarRight.init();
        this.GoogleMap.init();
    };
})
(jQuery);

//Initialization
telecomApp.init();